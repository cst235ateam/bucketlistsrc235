package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.Login;

@ManagedBean(name="FormController")
@ViewScoped

public class FormController {

	public FormController(){	
		
	}//end constructor
	
	
	
	//onFlash is run when the user hits flash from the TestForm xhtml page.
	//Input: user context.
	//Output: String that is the name of the TestResponse page.  
	public String onSubmit(Login login){
		//For debugging
		System.out.println("This is On Flash Entry");
		System.out.println(login.getuserName());
		System.out.println(login.getpassword());		
		
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("login",login);
		String ret="LoginResponse.xhtml";
		return ret;
	}
	
	
		
}
