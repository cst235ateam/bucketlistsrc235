package beans;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@ManagedBean(name="Login")
@ViewScoped

public class Login {
@NotNull()
@Size(min=5, max=15)
	private String userName;
@NotNull()
@Size(min=5, max=15)
	private String password;
		
	public Login(){	
		userName = "";
		password = "";
		
	}//end constructor
	
	public String getuserName(){
		return userName;
	}
	
	public String getpassword() {
		return password;
	}
	
	public void setuserName(String un){
		userName=un;
		System.out.println("userName is now:" + userName );
	}
	
	public void setpassword(String pw) {
		password = pw;
		System.out.println("Password is now:" + password );
	}
	
	
}
